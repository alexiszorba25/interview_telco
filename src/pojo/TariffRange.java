package pojo;

import java.time.DayOfWeek;
import java.time.LocalTime;
import interview_telco.Telco.Tariff;

/**
 * TariffRange class
 * @author Spiraquis Alexis
 */
public class TariffRange {
    private short tariffRangeId;
    private DayOfWeek tariffRangeInitDayOfWeek;
    private LocalTime tariffRangeInitTime;
    private LocalTime tariffRangeFinalTime;
    private Tariff tariff;

    public TariffRange() {
    }

    public TariffRange(short tariffRangeId, DayOfWeek tariffRangeInitDayOfWeek, LocalTime tariffRangeInitTime, LocalTime tariffRangeFinalTime, Tariff tariff) {
        this.tariffRangeId = tariffRangeId;
        this.tariffRangeInitDayOfWeek = tariffRangeInitDayOfWeek;
        this.tariffRangeInitTime = tariffRangeInitTime;
        this.tariffRangeFinalTime = tariffRangeFinalTime;
        this.tariff = tariff;
    }

    public short getTariffRangeId() {
        return tariffRangeId;
    }

    public DayOfWeek getTariffRangeInitDayOfWeek() {
        return tariffRangeInitDayOfWeek;
    }

    public LocalTime getTariffRangeInitTime() {
        return tariffRangeInitTime;
    }

    public LocalTime getTariffRangeFinalTime() {
        return tariffRangeFinalTime;
    }


    public Tariff getTariff() {
        return tariff;
    }
    
    
}
