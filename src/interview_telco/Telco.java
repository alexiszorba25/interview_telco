package interview_telco;

import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.DayOfWeek.THURSDAY;
import static java.time.DayOfWeek.TUESDAY;
import static java.time.DayOfWeek.WEDNESDAY;
import pojo.Call;
import pojo.Client;
import pojo.TariffRange;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import static interview_telco.Telco.Tariff.NIGHT;
import static interview_telco.Telco.Tariff.REGULAR;
import static interview_telco.Telco.Tariff.WEEKEND;
import static pojo.Client.ClientType.EXISTING;

/**
 * Interview Exercise - Telecommunications company<br>
 * Main class where methods with the algorithm logic are declared.<br>
 * On the pojo folder there are the object classes
 * @author Spiraquis Alexis
 * @version v1.0
 * @see Call
 * @see Client
 * @see Tariff
 * @see TariffRange
 */
public class Telco {
    private static final ArrayList<TariffRange> TARIFF_TABLE = new ArrayList<>();
    private static ArrayList<Call> calls = new ArrayList<>();
    /**
     * Structure with the different tariffs<br>
     * REGULAR: 0.05 pence per minute<br>
     * NIGHT: 0.02 pence per minute<br>
     * WEEKEND: 0.01 pence per minute
     */
    public enum Tariff{
        REGULAR(0.05),
        NIGHT(0.02),
        WEEKEND(0.01);
        private final double amount;
        Tariff(double amount){
            this.amount = amount;
        }
        public double getAmount(){
            return amount;
        }
    }
    /**
     * This is the main method created just for example purposes.<br>
     * First of all the ArrayList with all the possible ranges of tariff ranges is filled.<br>
     * Then the getTotalCharge is called with a client as a parameter.<br>
     * For the example an empty Client was used.
     * @param args the command line arguments
     * @see loadTariffTable
     * @see getTotalCharge
     */
    public static void main(String[] args) {
        loadTariffTable();
        Client client1 = new Client(1, "Alexis", EXISTING);
        loadCallsList(client1);
        client1.setCallList(calls);
        System.out.println("Total Charge for Client "+client1.getName()+":"+getTotalCharge(client1));
    }
     /**
     * The method receives a Client and loops through the list of calls.<br>
     * For each call it obtains the final charge and if it is an International call,
     * it doubles the result.<br>
     * Finally, it returns the sum of charges.<br>
     * @param client Client
     * @return Totalcharge for the clients calls.
     * @see getRangeCharge
     */
    public static double getTotalCharge(Client client){
        double totalCharge = 0;
        double partialCharge;
        for (Call call: client.getCallList()){
            partialCharge = getRangeCharge(call.getCallInitDate(),call.getCallFinalDate(), client.isNewClient());
            if (call.isInternational()){
                partialCharge *=2;
            }
            totalCharge += partialCharge;
        }
        return totalCharge;
    }
     /**
     * It´s a recursive method that receives an initial and final date of a range.<br>
     * First of all calling getTariffByInitDate obtains the initial Tariff Range.<br>
     * Then obtains the final DateTime of the range.<br>
     * If the tariff finalizes after the given finalDate, returns zero.<br>
     * If not, it calls itself but beginning the range one second after the actual 
     * initial time and adds the tarrif amount multiplied by the minutes.<br>
     * @param initDate Initial DateTime of the traffic range
     * @param finalDate Final DateTime of the traffic range
     * @param newClient True if the client is a New Client
     * @return Tariff charge or calls recursively
     * @see getTariffByInitDate
     */
    public static double getRangeCharge(LocalDateTime initDate, LocalDateTime finalDate, boolean newClient){
        TariffRange tariffRange = getTariffByInitDate(initDate);
        LocalDateTime finalRangeDateTime = initDate.with(tariffRange.getTariffRangeFinalTime()); 
        Tariff tariff;
        double amount;
            tariff = tariffRange.getTariff();
            if (tariff==REGULAR&&newClient){
                amount = NIGHT.getAmount();
            }else{
                amount = tariff.getAmount();
            }
        if (!finalRangeDateTime.isAfter(finalDate)){
            return getRangeCharge(finalRangeDateTime.plusSeconds(1),finalDate,newClient)
                    +amount*ChronoUnit.MINUTES.between(initDate, finalRangeDateTime);
        }else{
            return amount*ChronoUnit.MINUTES.between(initDate, finalDate);
        }
    }
     /**
     * The method receives an initial DateTime and returns the current TariffRange object.<br>
     * It loops through the TARIFF_TABLE ArrayList and returns the TariffRange object when
     * it matches the same WeekDay and the Time is between the initial and final time.<br>
     * @param initDate Initial LocalDate 
     * @return  TariffRange obtained
     */    
    public static TariffRange getTariffByInitDate(LocalDateTime initDate){
        for (TariffRange tariffRange: TARIFF_TABLE){
            if (initDate.getDayOfWeek()==tariffRange.getTariffRangeInitDayOfWeek()
                    &&(initDate.toLocalTime().isAfter(tariffRange.getTariffRangeInitTime())
                        ||(initDate.toLocalTime().equals(tariffRange.getTariffRangeInitTime())))
                    &&(initDate.toLocalTime().isBefore(tariffRange.getTariffRangeFinalTime())
                        ||initDate.toLocalTime().equals(tariffRange.getTariffRangeFinalTime())))
            {
                return tariffRange;
            }
        }
        return null; //The method will never get this line as all the possibilities will be covered at the TariffTable
    }
     /**
     * The method is declared for example purposes.<br>
     * It contains the list of TariffRange objects with all of the possible ranges 
     * of times and dates with their tariff.<br>
     * To simplify the algorithm logic, the ranges are splitted so as to have
     * the initial and final time on the same day completing a total of 17 ranges.
     */  
    public static void loadTariffTable(){
        TARIFF_TABLE.add(new TariffRange((short)1,MONDAY,LocalTime.of(0, 0, 0),LocalTime.of(3, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)2,MONDAY,LocalTime.of(4, 0, 0),LocalTime.of(21, 59, 59),REGULAR));
        TARIFF_TABLE.add(new TariffRange((short)3,MONDAY,LocalTime.of(22, 0, 0),LocalTime.of(23, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)4,TUESDAY,LocalTime.of(0, 0, 0),LocalTime.of(3, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)5,TUESDAY,LocalTime.of(4, 0, 0),LocalTime.of(21, 59, 59),REGULAR));
        TARIFF_TABLE.add(new TariffRange((short)6,TUESDAY,LocalTime.of(22, 0, 0),LocalTime.of(23, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)7,WEDNESDAY,LocalTime.of(0, 0, 0),LocalTime.of(3, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)8,WEDNESDAY,LocalTime.of(4, 0, 0),LocalTime.of(21, 59, 59),REGULAR));
        TARIFF_TABLE.add(new TariffRange((short)9,WEDNESDAY,LocalTime.of(22, 0, 0),LocalTime.of(23, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)10,THURSDAY,LocalTime.of(0, 0, 0),LocalTime.of(3, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)11,THURSDAY,LocalTime.of(4, 0, 0),LocalTime.of(21, 59, 59),REGULAR));
        TARIFF_TABLE.add(new TariffRange((short)12,THURSDAY,LocalTime.of(22, 0, 0),LocalTime.of(23, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)13,FRIDAY,LocalTime.of(0, 0, 0),LocalTime.of(3, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)14,FRIDAY,LocalTime.of(4, 0, 0),LocalTime.of(21, 59, 59),REGULAR));
        TARIFF_TABLE.add(new TariffRange((short)15,FRIDAY,LocalTime.of(22, 0, 0),LocalTime.of(23, 59, 59),NIGHT));
        TARIFF_TABLE.add(new TariffRange((short)16,SATURDAY,LocalTime.of(0, 0, 0),LocalTime.of(23, 59, 59),WEEKEND));
        TARIFF_TABLE.add(new TariffRange((short)17,SUNDAY,LocalTime.of(0, 0, 0),LocalTime.of(23, 59, 59),WEEKEND));
    }
     /**
     * The method is declared for example purposes.<br>
     * It loads an ArrayList of calls to test the algorithm.
     * @param client Example client
     */  
    public static void loadCallsList(Client client){
        calls.add(new Call(1, LocalDateTime.of(2018, 7, 20, 3, 58, 0), 
                LocalDateTime.of(2018, 7, 20, 4, 2, 0), false, client));//2*0,02+2*0,05 if existing
        calls.add(new Call(2, LocalDateTime.of(2018, 7, 21, 18, 0, 0), 
                LocalDateTime.of(2018, 7, 21, 18, 2, 0), false, client));//2*0,01
        calls.add(new Call(3, LocalDateTime.of(2018, 7, 20, 23, 58, 0), 
                LocalDateTime.of(2018, 7, 21, 0, 2, 0), false, client));//2*0,02+2*0,01
        calls.add(new Call(4, LocalDateTime.of(2018, 7, 21, 22, 58, 0), 
                LocalDateTime.of(2018, 7, 21, 23, 2, 0), true, client));//4*0,02 International
    }
}
