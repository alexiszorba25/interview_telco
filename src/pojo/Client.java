package pojo;

import java.util.List;

/**
 * Client class
 * @author Spiraquis Alexis
 */
public class Client {
    private int clientId;
    private String name;
    private ClientType clientType;
    private List<Call> callList;

    public void setCallList(List<Call> callList) {
        this.callList = callList;
    }

    public Client() {
    }

    public Client(int clientId, String name, ClientType clientType) {
        this.clientId = clientId;
        this.name = name;
        this.clientType = clientType;
    }

    public List<Call> getCallList() {
        return callList;
    }
    
    public int getClientId() {
        return clientId;
    }

    public String getName() {
        return name;
    }

    public ClientType getClientType() {
        return clientType;
    }
    public boolean isNewClient(){
        return clientType==ClientType.NEW;
    }
    public enum ClientType{
        NEW,EXISTING;
    }
}
