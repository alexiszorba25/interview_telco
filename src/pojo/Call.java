package pojo;

import java.time.LocalDateTime;


/**
 * Call class
 * @author Spiraquis Alexis
 */
public class Call {
    private double callId;
    private LocalDateTime callInitDate;
    private LocalDateTime callFinalDate;
    private boolean international;
    private Client clientCall;

    public Call() {
    }

    public Call(double callId, LocalDateTime callInitDate, LocalDateTime callFinalDate, boolean international, Client clientCall) {
        this.callId = callId;
        this.callInitDate = callInitDate;
        this.callFinalDate = callFinalDate;
        this.international = international;
        this.clientCall = clientCall;
    }

    public boolean isInternational() {
        return international;
    }

    public double getCallId() {
        return callId;
    }

    public LocalDateTime getCallInitDate() {
        return callInitDate;
    }

    public LocalDateTime getCallFinalDate() {
        return callFinalDate;
    }

    public Client getClientCall() {
        return clientCall;
    }   
}
